Benchmarks
========

Benchmarks of JSON-RPC routers.

Current results:

```
name \ time/op    pjrpcpjson.txt  pjrpcgogen.txt  osamingo.txt
Hello-4              17.7µs ± 0%     17.9µs ± 0%   11.6µs ± 0%
ManyParams-4         31.4µs ± 0%     31.0µs ± 0%   15.8µs ± 0%
[Geo mean]           23.5µs          23.6µs        13.5µs     

name \ alloc/op   pjrpcpjson.txt  pjrpcgogen.txt  osamingo.txt
Hello-4              3.35kB ± 0%     3.45kB ± 0%   3.31kB ± 0%
ManyParams-4         4.55kB ± 0%     4.69kB ± 0%   4.34kB ± 0%
[Geo mean]           3.90kB          4.02kB        3.79kB     

name \ allocs/op  pjrpcpjson.txt  pjrpcgogen.txt  osamingo.txt
Hello-4                36.0 ± 0%       36.0 ± 0%     32.0 ± 0%
ManyParams-4           46.0 ± 0%       46.0 ± 0%     33.0 ± 0%
[Geo mean]             40.7            40.7          32.5     
```
