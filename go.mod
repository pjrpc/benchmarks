module gitlab.com/pjrpc/benchmarks

go 1.15

require (
	github.com/goccy/go-json v0.4.8
	github.com/golang/protobuf v1.4.3
	github.com/google/uuid v1.1.2
	github.com/osamingo/jsonrpc/v2 v2.4.0
	gitlab.com/pjrpc/pjrpc v1.5.0
	google.golang.org/protobuf v1.25.0
)
