// Package runner contains method to run tests of http servers and runner of benchmarks.
package runner

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/pjrpc/pjrpc/model"
)

type requestTest struct {
	req            *http.Request
	expectedBody   string
	expectedStatus int
}

func createHTTPRequest(body string) *http.Request {
	req, err := http.NewRequestWithContext(
		context.Background(),
		http.MethodPost,
		"http://127.0.0.1:8080/rpc",
		bytes.NewBuffer([]byte(body)),
	)
	if err != nil {
		panic("http.NewRequestWithContext: " + err.Error())
	}

	req.Header.Set("Content-Type", "application/json")

	return req
}

func createHelloRequest() *http.Request {
	return createHTTPRequest(
		`{"jsonrpc":"2.0","id":"1","method":"hello","params":{"name":"rpc client"}}`,
	)
}

func createManyParamsRequest() *http.Request {
	return createHTTPRequest(
		`{"jsonrpc":"2.0","id":"2","method":"many_params","params":{"name":"name","age":22,"options":{"price":123.23,"value":1234567890,"value_big":12345678901234567890,"is_good":true,"description":"Some text"}}}`,
	)
}

func createTestRequest(req *http.Request, expBody string) *requestTest {
	res := &requestTest{
		req:            req,
		expectedBody:   expBody,
		expectedStatus: http.StatusOK,
	}

	return res
}

func getDefaultRequestList() []*requestTest {
	return []*requestTest{
		createTestRequest(
			createHelloRequest(),
			`{"jsonrpc":"2.0","id":"1","result":{"name":"rpc client"}}`,
		),
		createTestRequest(
			createManyParamsRequest(),
			`{"jsonrpc":"2.0","id":"2","result":{"name":"name","age":22,"options":{"price":123.23,"value":1234567890,"value_big":12345678901234567890,"is_good":true,"description":"Some text"}}}`,
		),
	}
}

func runRequests(t *testing.T, h http.Handler, requests []*requestTest) {
	for _, req := range requests {
		rec := httptest.NewRecorder()

		h.ServeHTTP(rec, req.req)

		if req.expectedStatus != rec.Code {
			t.Fatalf("expected status != status:\nwant: %d\ngot:  %d", req.expectedStatus, rec.Code)
		}

		resp := new(model.Response)
		err := json.NewDecoder(rec.Body).Decode(&resp)
		if err != nil {
			t.Fatal("failed to decode body:", err)
		}

		res, err := json.Marshal(resp)
		if err != nil {
			t.Fatal("failed to Marshal body:", err)
		}

		if req.expectedBody != string(res) {
			t.Fatalf("expected body != body:\nwant: '%s'\ngot:  '%s'", req.expectedBody, res)
		}
	}
}

func RunHandlerTests(t *testing.T, h http.Handler) {
	runRequests(t, h, getDefaultRequestList())
}

func runBench(b *testing.B, h http.Handler, createReqFunc func() *http.Request) {
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		b.StopTimer()
		rec := httptest.NewRecorder()
		req := createReqFunc()

		b.StartTimer()
		h.ServeHTTP(rec, req)
	}
}

func RunBenchHello(b *testing.B, h http.Handler) {
	runBench(b, h, createHelloRequest)
}

func RunBenchManyParams(b *testing.B, h http.Handler) {
	runBench(b, h, createManyParamsRequest)
}
