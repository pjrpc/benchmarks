.PHONY: proto test bench bench_save print_cmp

proto:
	# generates test protobuf files
	protoc \
		--go_out=./proto/gogen/ --go_opt=paths=source_relative \
		--go-pjrpc_out=./proto/gogen/ --go-pjrpc_opt=paths=source_relative \
		proto/server.proto
	
	protoc \
		--go-pjson_out=./proto/pjson/ --go-pjson_opt=paths=source_relative \
		--go-pjrpc_out=./proto/pjson/ --go-pjrpc_opt=paths=source_relative \
		proto/server.proto

test:
	go test -v ./server/...

bench:
	go test -bench=. -benchmem ./server/...
	
bench_save:
	go test -bench=. -benchmem ./server/pjrpcpjson > ./results/pjrpcpjson.txt
	go test -bench=. -benchmem ./server/pjrpcgogen > ./results/pjrpcgogen.txt
	go test -bench=. -benchmem ./server/osamingo > ./results/osamingo.txt

print_cmp:
	cd ./results/ && benchstat -geomean -split=goos,goarch pjrpcpjson.txt pjrpcgogen.txt osamingo.txt
