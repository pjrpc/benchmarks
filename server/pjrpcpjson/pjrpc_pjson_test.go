package pjrpcpjson_test

import (
	"testing"

	"gitlab.com/pjrpc/benchmarks/runner"
	"gitlab.com/pjrpc/benchmarks/server/pjrpcpjson"
)

func TestServer(t *testing.T) {
	srv := pjrpcpjson.New()
	runner.RunHandlerTests(t, srv)
}

func BenchmarkHello(b *testing.B) {
	h := pjrpcpjson.New()
	runner.RunBenchHello(b, h)
}

func BenchmarkManyParams(b *testing.B) {
	h := pjrpcpjson.New()
	runner.RunBenchManyParams(b, h)
}
