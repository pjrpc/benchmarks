package pjrpcpjson

import (
	"context"
	"net/http"

	"gitlab.com/pjrpc/pjrpc/server"

	"gitlab.com/pjrpc/benchmarks/proto/pjson/proto"
)

type PJRPCPJSON struct {
	srv *http.Server
}

func (*PJRPCPJSON) Hello(ctx context.Context, in *proto.HelloRequest) (*proto.HelloResponse, error) {
	return &proto.HelloResponse{Name: in.Name}, nil
}

func (*PJRPCPJSON) ManyParams(ctx context.Context, in *proto.ManyParamsRequest) (*proto.ManyParamsResponse, error) {
	resp := &proto.ManyParamsResponse{
		Name:    in.Name,
		Age:     in.Age,
		Options: in.Options,
	}

	return resp, nil
}

func New() http.Handler {
	srv := server.New()
	s := &PJRPCPJSON{}

	srv.OnPanic = nil

	proto.RegisterTestServiceServer(srv, s)

	return srv
}
