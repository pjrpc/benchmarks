package pjrpcgogen_test

import (
	"testing"

	"gitlab.com/pjrpc/benchmarks/runner"
	"gitlab.com/pjrpc/benchmarks/server/pjrpcgogen"
)

func TestServer(t *testing.T) {
	srv := pjrpcgogen.New()
	runner.RunHandlerTests(t, srv)
}

func BenchmarkHello(b *testing.B) {
	h := pjrpcgogen.New()
	runner.RunBenchHello(b, h)
}

func BenchmarkManyParams(b *testing.B) {
	h := pjrpcgogen.New()
	runner.RunBenchManyParams(b, h)
}
