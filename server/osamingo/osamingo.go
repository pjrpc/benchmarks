package osamingo

import (
	"context"
	"net/http"

	json "github.com/goccy/go-json"
	"github.com/osamingo/jsonrpc/v2"
)

type helloHandler struct{}

type helloRequest struct {
	Name string `json:"name"`
}

type helloResponse struct {
	Name string `json:"name"`
}

func (helloHandler) ServeJSONRPC(c context.Context, params *json.RawMessage) (interface{}, *jsonrpc.Error) {
	var r helloRequest
	if err := jsonrpc.Unmarshal(params, &r); err != nil {
		return nil, err
	}

	return &helloResponse{Name: r.Name}, nil
}

type additionalOptions struct {
	Price       float64 `json:"price"`
	Value       int64   `json:"value"`
	ValueBig    uint64  `json:"value_big"`
	IsGood      bool    `json:"is_good"`
	Description string  `json:"description"`
}

type manyParamsHandler struct{}

type manyParamsRequest struct {
	Name    string             `json:"name"`
	Age     int32              `json:"age"`
	Options *additionalOptions `json:"options"`
}

type manyParamsResponse struct {
	Name    string             `json:"name"`
	Age     int32              `json:"age"`
	Options *additionalOptions `json:"options"`
}

func (manyParamsHandler) ServeJSONRPC(c context.Context, params *json.RawMessage) (interface{}, *jsonrpc.Error) {
	var r manyParamsRequest
	if err := jsonrpc.Unmarshal(params, &r); err != nil {
		return nil, err
	}

	resp := &manyParamsResponse{
		Name:    r.Name,
		Age:     r.Age,
		Options: r.Options,
	}

	return resp, nil
}

func New() http.Handler {
	mr := jsonrpc.NewMethodRepository()

	if err := mr.RegisterMethod("hello", helloHandler{}, &helloRequest{}, &helloResponse{}); err != nil {
		panic(err)
	}

	if err := mr.RegisterMethod("many_params", manyParamsHandler{}, &manyParamsRequest{}, &manyParamsResponse{}); err != nil {
		panic(err)
	}

	return mr
}
