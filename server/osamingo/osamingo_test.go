package osamingo_test

import (
	"testing"

	"gitlab.com/pjrpc/benchmarks/runner"
	"gitlab.com/pjrpc/benchmarks/server/osamingo"
)

func TestServer(t *testing.T) {
	srv := osamingo.New()
	runner.RunHandlerTests(t, srv)
}

func BenchmarkHello(b *testing.B) {
	h := osamingo.New()
	runner.RunBenchHello(b, h)
}

func BenchmarkManyParams(b *testing.B) {
	h := osamingo.New()
	runner.RunBenchManyParams(b, h)
}
